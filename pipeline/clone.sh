#!/bin/bash

# Curr Dir
cd "${0%/*}"
cd ../..

git clone -b develop git@gitlab.com:lee_dissertation/phase-two/base.git
git clone -b develop git@gitlab.com:lee_dissertation/phase-two/lerring_framework.git
git clone -b develop git@gitlab.com:lee_dissertation/phase-two/test_runner.git

