#!/bin/bash

# Curr Dir
cd "${0%/*}"
cd ../..

docker-compose -f base/docker-compose.yml down
docker-compose -f lerring_framework/docker-compose.yml down
docker-compose -f test_runner/docker-compose.yml down

rm -rf base
rm -rf lerring_framework
rm -rf test_runner

