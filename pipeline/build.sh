#Builds all applications
cd "${0%/*}"
cd ../..

docker-compose -f base/docker-compose.yml build
docker-compose -f lerring_framework/docker-compose.yml build
docker-compose -f test_runner/docker-compose.yml build
